const archiver = require('archiver')
const AWS = require('aws-sdk')
const stream = require('stream')
const got = require('got');
const crypto = require('crypto');
const EventEmitter = require('events');

const S3_JSON_PREFIX = 'assetList/';
const S3_ZIP_PREFIX = 'download/';

/**
 * Enrich the payload with  asset size and split it
 * @param payload
 * @returns {Promise<void>}
 */
async function enrichAssetList(payload) {
    console.log("received payload " + JSON.stringify(payload))

    const s3 = new AWS.S3()

    const assetList = await getAssetList(payload.hash, process.env.BD_ZIP_BUCKET, s3);
    assetList.errors = assetList.errors || []; //adding error array if doesn't exist
    let enrichedPayload = await fetchAssetSize(assetList);
    enrichedPayload = await splitPayload(enrichedPayload);

    payload.zipList = enrichedPayload.zipList.map(zip => ({zipName: zip.zipName, size: zip.size}));

    //update the list in s3
    await putAssetList(enrichedPayload, payload.hash, process.env.BD_ZIP_BUCKET, s3);
    return payload;
}

/**
 * Will enrich the payload with the size of the assets
 * through HEAD requests run in parallel with events
 * @param payload
 * @returns {Promise<void>}
 */
async function fetchAssetSize(payload) {
    console.log("received payload " + JSON.stringify(payload));
    let sizes = [];
    let requestFinishedEmitter = new EventEmitter();
    let assetsQueue = [...payload.assets];

    //when event is triggered with un-queuing an elem from assetQueue and fetch its size
    requestFinishedEmitter.on('fetchSizeRequest', () => {
        if (assetsQueue.length > 0) {
            let item = assetsQueue.shift();
            if (item.size && parseInt(item.size) > 0) {
                sizes.push(parseInt(item.size));
                requestFinishedEmitter.emit('fetchSizeRequest');
            } else {
                console.log(`start HEAD request on ${item.url}`);
                const request = got(item.url, {method: "HEAD"}).then(res => {
                    if (res.headers['content-length']) {
                        return parseInt(res.headers['content-length'])
                    } else {
                        return 0
                    }
                }).catch((err) => {
                    console.error(`could not fetch size for ${item.url}: ${err.message}`)
                    payload.errors.push({name: item.name, message: err.message})
                    return 0;
                }).finally(() => {
                    requestFinishedEmitter.emit('fetchSizeRequest');
                });
                sizes.push(request);
            }
        } else {
            requestFinishedEmitter.emit('fetchSizeEnd');
        }
    });

    //launch 30 requests
    for (let i = 0; i < 30; i++) {
        requestFinishedEmitter.emit('fetchSizeRequest');
    }

    //return a promise that resolves when the end event is called, ie no more items in the queue
    return new Promise(resolve => {
        requestFinishedEmitter.on('fetchSizeEnd', async () => {
            sizes = await Promise.all(sizes);
            payload.totalSize = sizes.reduce((a, b) => a + b, 0)
            payload.assets = payload.assets.map((asset, index) => {
                asset.size = sizes[index]
                return asset
            }).filter(asset => asset.size > 0);
            resolve(payload);
        });
    });
}

/**
 * Check the assets size, split downloads into different archives
 * depending on ARCHIVE SIZE
 * @param payload
 * @returns {Promise<[]>}
 */
async function splitPayload(payload) {
    console.log("received payload " + JSON.stringify(payload))

    const maxSize = parseInt(process.env.BD_ZIP_SIZE)
    const maxAssetPerZip = parseInt(process.env.BD_ASSET_PER_ZIP)

    //make a hash id out of filenames and sizes
    let someSignatureData = "";
    payload.assets.forEach((asset) => {someSignatureData += asset.name + asset.size.toString()});
    const hashID = "download_" + crypto.createHash('md5').update(someSignatureData).digest("hex");

    let sizeCounter = 0
    let nbAssetCounter = 0
    let assetsAccumulator = []
    let zipCount = 1
    let zipList = []

    //loop on assets url
    payload.assets.forEach((asset, index) => {
        sizeCounter += asset.size
        nbAssetCounter++;
        assetsAccumulator.push(asset)
        if (sizeCounter > maxSize || (nbAssetCounter % maxAssetPerZip === 0) || (index + 1 === payload.assets.length)) { //until we reach our size threshold or end of array
            const zipName = (sizeCounter < payload.totalSize || zipCount > 1) ? hashID + "-" + zipCount++ + ".zip" : hashID + ".zip"
            //send a sublist of assets to a zip lambda
            const sublist = {assets: assetsAccumulator, zipName: zipName, size: sizeCounter}
            zipList.push(sublist)
            //reset asset list and size counter
            assetsAccumulator = []
            sizeCounter = 0
        }
    })

    payload.zipList = zipList
    delete payload.assets //remove the asset list as they are in the zip list now
    return payload
}

/**
 * Check if zip files have already been created for that bulk download request
 * @param payload
 * @returns {Promise<void>}
 */
async function checkIfZipsExist(payload) {
    console.log("received payload " + JSON.stringify(payload))

    const s3 = new AWS.S3()

    //we check if the zip files with the same hash already exist
    const zipPrefix = payload.zipList[0].zipName.replace(/(-\d)?\.zip/, "");
    const objectList = await s3.listObjects({
        Bucket: process.env.BD_ZIP_BUCKET,
        Prefix: S3_ZIP_PREFIX + zipPrefix
    }).promise();

    console.log(`found ${objectList.Contents.length} objects with the prefix ${zipPrefix}`);

    payload.zipsExist = 1
    //we compare the number of zip found
    if (objectList.Contents.length === payload.zipList.length) {
        objectList.Contents.forEach((content, index) => {
            //we compare the size of each zip found and the payload.
            //if it differs too much, we assume they're different
            const round = Math.round(payload.zipList[index].size / content.Size);
            console.log('expected zip size: ' + payload.zipList[index].size + ' / found in s3: ' + content.Size + " = " + round);
            if (round !== 1) {
                payload.zipsExist = 0;
            }
        })
    } else {
        payload.zipsExist = 0;
        if (objectList.Contents.length > 0) {
            await s3.deleteObjects({
                Bucket: process.env.BD_ZIP_BUCKET,
                Delete: {
                    Objects: objectList.Contents.map(item => ({Key: item.Key}))
                }
            }).promise()
        }
    }

    return payload
}

/**
 * streams binary data from URL
 * pipe it into archiver then directly to s3
 *
 * inspired from here https://dev.to/lineup-ninja/zip-files-on-s3-with-aws-lambda-and-node-1nm1
 *
 * @param payload
 * @returns Promise<ManagedUpload.SendData> location
 */
async function generateZipStream(payload) {
    console.log("received payload " + JSON.stringify(payload))

    const s3 = new AWS.S3()

    const assetList = await getAssetList(payload.hash, process.env.BD_ZIP_BUCKET, s3);
    const splitZip = assetList.zipList.find(zip => zip.zipName === payload.zip.zipName)

    console.log("generating zip of the assets: " + JSON.stringify(splitZip.assets));

    const s3BodyPassThroughStream = new stream.PassThrough()

    const s3Upload = s3.upload({
        Body: s3BodyPassThroughStream,
        Bucket: process.env.BD_ZIP_BUCKET,
        ContentType: 'application/zip',
        Key: S3_ZIP_PREFIX + splitZip.zipName,
    })

    s3Upload.on('httpUploadProgress', (progress) => {
        console.log(progress)
    })


    const archive = archiver("zip", {})

    archive
        .on('entry', (event) => console.log(`archive ${splitZip.zipName} entry ${JSON.stringify(event)}`))
        .on('progress', (event) => console.log(`archive ${splitZip.zipName} progress ${JSON.stringify(event)}`))
        .on('warning', (event) => console.log(`archive ${splitZip.zipName} warning ${event}`))
        .on('finish', () => console.log(`archive ${splitZip.zipName} finish`))
        .on('end', () => console.log(`archive ${splitZip.zipName} end`))
        .on('close', (event) => console.log(`archive ${splitZip.zipName} close ${event}`))
        .on('error', (err) => {
            console.error('archive', err);
            throw err;
        });

    s3BodyPassThroughStream
        .on('pipe', () => console.log(`stream pipe`))
        .on('unpipe', () => console.log(`stream unpipe`))
        .on('close', () => console.log(`stream close`))
        .on('error', (err) => {
            console.error('archive', err);
            throw err;
        })
        .on('end', () => console.log(`stream end`))
        .on('finish', () => console.log(`stream finish`))

    archive.pipe(s3BodyPassThroughStream)

    for (let asset of splitZip.assets) {
        archive.append(getStream(asset.url), {name: asset.name});
    }

    archive.finalize()
    return s3Upload.promise().then((upload) => {
        payload.s3upload = upload;
        return payload
    })
}

/**
 * PassThrough wrapper that creates the actual stream
 * when necessary to prevent timeout
 * @param url
 * @returns {Stream}
 */
function getStream(url) {
    let streamCreated = false;
    const passThroughStream = new stream.PassThrough();
    passThroughStream.on('newListener', event => {
        if (!streamCreated && event === 'data') {
            console.log(`create stream for path ${url}`);
            const httpStream = got.stream(url);  // <<== stream is created here
            httpStream
                .on('error', err => passThroughStream.emit('error', err))
                .on('finish', () => console.log(` finish stream for path ${url}`))
                .on('close', () => console.log(`stream close`))
                .pipe(passThroughStream);
            streamCreated = true;
        }
    });
    return passThroughStream;
}

/**
 * Generate s3 presigned urls
 * @param payload
 * @returns {Promise<*>}
 */
async function generateUrl(payload) {
    console.log("received payload " + JSON.stringify(payload))

    const s3 = new AWS.S3()

    const assetList = await getAssetList(payload.hash, process.env.BD_ZIP_BUCKET, s3);
    for (let zip of assetList.zipList) {
        zip.url = s3.getSignedUrl('getObject', {
            Bucket: process.env.BD_ZIP_BUCKET,
            Key: S3_ZIP_PREFIX + zip.zipName,
            Expires: parseInt(process.env.BD_LINK_EXPIRES)
        });
    }
    //update the list in s3
    return putAssetList(assetList, payload.hash, process.env.BD_ZIP_BUCKET, s3).then(() => payload)
}

/**
 * This function will build a message to send to SNS
 * Exceptions are also routed to this function
 * @param payload: actually take the hash from the State Machine context object in case there was an exception
 * @returns {Promise<PromiseResult<SNS.PublishResponse, AWSError>>}
 */
async function sendNotificationToSNS(payload) {
    console.log("received payload " + JSON.stringify(payload));

    //SNS has 256kb limit, if we have too many pre-signed URL, we might exceed it
    //So we just pass the reference of the file containing the links
    const message = {
        Bucket: process.env.BD_ZIP_BUCKET,
        Key: S3_JSON_PREFIX + payload.hash + ".json"
    }

    //If there was an exception, we also append it to the message
    if (payload.input.Error) {
        message.exception = payload.input;
    }

    const sns = new AWS.SNS()
    return sns.publish({
        Message: JSON.stringify(message),
        TopicArn: process.env.BD_TOPIC
    }).promise().then(() => {
        if (payload.input.Error) {
            //if there was an exception then we make the execution fail
            throw new Error("The bulk download workflow execution failed: " + JSON.stringify(payload.input.Error));
        } else {
            return message;
        }
    });
}

/**
 * get an asset list from s3 and parse it to json
 * @param hash
 * @param s3
 * @param bucketName
 * @returns {Promise<PromiseResult<D, E>>}
 */
async function getAssetList(hash, bucketName, s3) {
    return s3.getObject({
        Bucket: bucketName,
        Key: S3_JSON_PREFIX + hash + ".json"
    }).promise().then((request => {
        const assetList = JSON.parse(request.Body.toString());
        console.log(`fetch asset list from s3 ${JSON.stringify(assetList)}`)
        return assetList
    })).catch((err => {
        console.error(err);
        throw err;
    }));
}

/**
 * get an asset list from s3 and parse it to json
 * @param hash
 * @param s3
 * @param bucketName
 * @returns {Promise<PromiseResult<D, E>>}
 */
async function putAssetList(assetList, hash, bucketName, s3) {
    return s3.upload({
        Body: JSON.stringify(assetList),
        Bucket: process.env.BD_ZIP_BUCKET,
        ContentType: 'application/json',
        Key: S3_JSON_PREFIX + hash + ".json",
    }).promise().then((upload) => {
        console.log(`uploading asset list to s3 ${JSON.stringify(assetList)}`);
        return upload;
    });
}

module.exports = {
    generateZipStream: generateZipStream,
    checkIfZipsExist: checkIfZipsExist,
    generateUrl: generateUrl,
    sendNotificationToSNS: sendNotificationToSNS,
    enrichAssetList: enrichAssetList,
    fetchAssetSize: fetchAssetSize,
    splitPayload: splitPayload
}
