const apiHandler = require('../../bulkdl-api-handler.js');
const stepFunctions = require('../../bulkdl-step-functions.js');
const eventBadSchema = require('../../../stubs/api_event_bad_schema.json');
const eventBadJson = require('../../../stubs/api_event_bad_json.json');
const payloadRaw = require('../../../stubs/payload_raw.json');
const payloadEnriched = require('../../../stubs/payload_enriched.json');
const payloadSplit = require('../../../stubs/payload_split.json');
const payloadEnriched5Assets = require('../../../stubs/payload_enriched_5_assets.json');
const payloadSplit5Assets = require('../../../stubs/payload_split_5_assets.json');
const payloadEnrichedOneAsset = require('../../../stubs/payload_enriched_one_asset.json');
const payloadSplitOneAsset = require('../../../stubs/payload_split_one_asset.json');
const mockListObjectsResponse = require('../../../stubs/s3_list_objects_stub.json');
const mockUploadResponse = require('../../../stubs/s3_upload_response_stub.json');
const event = require('../../../stubs/api_event.json')

jest.mock('got', () => {
    const response = {headers: {'content-length': '1000'}}
    return jest.fn( (arg) => {
        if (arg === "http://error.404.mp4") {
            return Promise.reject(new Error("404"));
        } else return Promise.resolve(response);
    })
})
jest.mock('aws-sdk', () => {
    return {
        S3: jest.fn(() => ({
            upload: jest.fn(() => ({
                on: jest.fn(),
                promise: jest.fn( () => Promise.resolve(mockUploadResponse))
            })),
            listObjects: jest.fn(() => ({
                promise: jest.fn(() => Promise.resolve(mockListObjectsResponse))
            }))
        })),
        Lambda: jest.fn(() => ({
            invoke: jest.fn(() => ({
                promise: jest.fn()
            }))
        })),
        StepFunctions: jest.fn(() => ({
            startExecution: jest.fn(() => ({
                promise: jest.fn(() => Promise.resolve({executionArn: "myarn", startDate: "not sure"}))
            }))
        }))
    };
});
jest.mock('archiver', () => {
    return jest.fn(() => ({
        on: jest.fn(),
        pipe: jest.fn(),
        finalize: jest.fn(),
        append: jest.fn()
    }))
})
process.env.BD_ZIP_SIZE = "1000000000"

let context
describe('api handler validation', function () {
    it('verifies that payload with wrong schema is handled', async () => {
        const result = await apiHandler.apiHandler(eventBadSchema, context)
        expect(result.statusCode).toEqual(400)
        const responseBody = JSON.parse(result.body);
        expect(responseBody.message[0].message).toContain('requires property')
    })
    it('verifies that payload with incorrect json is handled', async () => {
        const result = await apiHandler.apiHandler(eventBadJson, context)
        expect(result.statusCode).toEqual(400)
        const responseBody = JSON.parse(result.body);
        expect(responseBody.message).toContain('Unexpected token')
    })
    it('verifies that correct json is processed', async () => {
        const result = await apiHandler.apiHandler(event, context)
        expect(result.statusCode).toEqual(200)
        const responseBody = JSON.parse(result.body);
        expect(responseBody.message.stepFunction.executionArn).toEqual("myarn")
    })
})

describe('enrich payload functions test', function () {
    it('verifies that payload is enriched with asset size and total size with fetchAssetSize()', async () => {
        const payload = await stepFunctions.fetchAssetSize(payloadRaw)
        expect(payload.assets[0].size).toEqual(1000);
        expect(payload.assets[2].size).toEqual(3999);
        expect(payload.totalSize).toEqual(5999);
        expect(payload.assets.length).toEqual(3);
        expect(payload.errors[0].name).toEqual("bbb_sunflower_native_60fps_normal.mp4")
    })
    it('verifies how it splits bulk dls', async () => {
        let payload = await stepFunctions.splitPayload(payloadEnriched)
        expect(payload).toEqual(payloadSplit)
    })
    it('verifies it doesnt split when only one asset', async () => {
        let payload = await stepFunctions.splitPayload(payloadEnrichedOneAsset)
        expect(payload).toEqual(payloadSplitOneAsset)
    })
    it('verifies it splits when limit of assets is reached', async () => {
        let payload = await stepFunctions.splitPayload(payloadEnriched5Assets)
        expect(payload).toEqual(payloadSplit5Assets)
    })
})

describe('check if zips exists', function () {
    it('check if the zips for this bulk download already exits.', async () => {
        const payload = await stepFunctions.checkIfZipsExist(payloadSplit)
        expect(payload.zipsExist).toEqual(1)
    })
})
