const AWS = require('aws-sdk');
const payloadSchema = require('./event.schema.json');
const validate = require('jsonschema').validate;
const crypto = require('crypto');

const S3_JSON_PREFIX = 'assetList/';

/**
 * Check that the request body format is correct and return error message if not
 * Otherwise triggers the zip master function
 * @param event payload
 * @param context
 * @returns {Promise<{body: string, statusCode: number}|*>}
 */
async function apiHandler(event, context) {
    let response
    let payload
    let jsonSchemaErrors = []

    console.log(`received event ${JSON.stringify(event)}`)
    try {
        payload = JSON.parse(event.body)
        let validation = validate(payload, payloadSchema)
        jsonSchemaErrors = validation.errors
    } catch (error) {
        console.warn("invalid payload : " + error.message)
        response = {
            'statusCode': 400,
            'body': JSON.stringify({
                message: error.message,
            })
        }
    }

    if (!response && jsonSchemaErrors.length > 0) {
        console.warn("invalid payload : " + JSON.stringify(jsonSchemaErrors))
        response = {
            'statusCode': 400,
            'body': JSON.stringify({
                message: jsonSchemaErrors,
            })
        }
    }

    if (!response) {
        try {
            payload.bucket = process.env.BD_ZIP_BUCKET

            const hashID = crypto.randomBytes(16).toString('hex');
            const assetListS3ObjKey = S3_JSON_PREFIX + hashID + '.json';

            //asset list might be too big to be passed directly to the state machine so we save it in s3 and pass the ref
            const s3 = new AWS.S3();
            let s3Upload = await s3.upload({
                Body: JSON.stringify(payload),
                Bucket: process.env.BD_ZIP_BUCKET,
                ContentType: 'application/json',
                Key: assetListS3ObjKey,
            }).promise();

            console.log(`uploaded asset list to s3 ${s3Upload.url}`)

            const stepFunctions = new AWS.StepFunctions();
            const stepFunctionData = await stepFunctions.startExecution({
                stateMachineArn: process.env.BD_STATE_MACHINE,
                input: JSON.stringify({
                    hash: hashID
                })
            }).promise();

            response = {
                'statusCode': 200,
                'body': JSON.stringify({
                    message: {
                        stepFunction: stepFunctionData
                    }
                })
            }
        } catch (err) {
            console.log(err)
            response = {
                'statusCode': 500,
                'body': JSON.stringify({
                    message: err.message,
                })
            }
        }
    }
    return response
}

module.exports = {
    apiHandler: apiHandler
}
