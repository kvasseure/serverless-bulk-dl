# asset-bulk-download
A simple AWS serverless apps that downloads files from one location, stream them to archiver, and directly stream out to a zip file on S3.
Since it's streaming, it doesn't take any tmp storage on the lambda, so the only limitation is the lambda timeout.
It's leveraging:
* AWS SAM
* Lambda (Nodejs)
* Step Functions
* SNS
* API Gateway

# deploy
Requires to install the aws SAM cli and node js.
Then run:
```
sam build
sam deploy --guided
```

# test
## unit tests
go to the /bulk-download folder and run
```
yarn && yarn test
```
tests are relying on the /stubs folder that contains examples of payload
## live test
